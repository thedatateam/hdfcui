import React, {Component} from 'react';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';

const theme = createMuiTheme({
  props: {
    // Name of the component ⚛️
    MuiButtonBase: {
      // The default props to change
      disableRipple: true, // No more ripple, on the whole application 💣!
    },
  },
  typography: {
    "fontFamily": "\"Raleway\", sans-serif",
    "fontSize": 14,
    "fontWeightLight": 300,
    "fontWeightRegular": 400,
    "fontWeightMedium": 500
   }
});

import Login from './login';

class ReactBootStrap extends Component{
    render() {
    //   const PrivateRoute = ({ component: Component, ...rest }) => (
    //     <Route {...rest} render={(props) => (
    //       true
    //         ? <Component {...props} />
    //         : <Redirect to='/mdmconfig/login' />
    //     )} />
    //   )
      const PublicRoute =  ({ component: Component, ...rest }) => (
        <Route {...rest} render={(props) => (
          false
            ? 
              <Redirect to='/hdfc' />
            :
            <Component {...props} />
        )} />
      )
      return(
        <ThemeProvider theme={theme}>
          <BrowserRouter>
              <Switch>
                  <Route exact path="/">
                      <Redirect from='/' to='/hdfc'/>
                  </Route>
                  <PublicRoute exact path="/hdfc" component={Login} />
                 </Switch>
          </BrowserRouter>
        </ThemeProvider>
      );
    }
  }
  export default ReactBootStrap;