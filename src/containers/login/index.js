import React from 'react';
import { Link } from 'react-router-dom';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import { connect } from "react-redux";
import cadenz from "../../assets/images/cadenz.png"
import Card from '@material-ui/core/Card';
import classNames from 'classnames';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import BasicTable from './table';
// import { addCount } from "./store/counter/actions";

const useStyles = makeStyles(theme => ({


}));

const Login = () => {
  const classes = useStyles();
  document.title = "HDFC";
  const [modalData, setModalData] = React.useState([]);
  const [selectedFile, setSelectedFile] = React.useState();
  const [isFilePicked, setIsFilePicked] = React.useState(false);
  const [isModaluploaded, setModaluploaded] = React.useState(false);

  const changeHandler = (event) => {
    console.log(event.target.files[0]);
    setSelectedFile(event.target.files[0].name);
    setIsFilePicked(true);
  };


  // const [age, setAge] = React.useState('');

  // const inputLabel = React.useRef(null);
  // const [labelWidth, setLabelWidth] = React.useState(0);
  // React.useEffect(() => {
  //   setLabelWidth(inputLabel.current.offsetWidth);
  // }, []);

  const handleModalChange = event => {
    fetch('http://127.0.0.1:8000/deploy')
      .then(results => results.json())
      .then(data => {
        setModalData({data})
        console.log(data);
      }).catch(err => {
        console.log(err);
      });
  }

  const handlefileChange = event => {
    const filePath={"path":selectedFile};
    fetch('http://127.0.0.1:8000/fileupload',{     headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },   body: JSON.stringify(filePath)  ,  method: "POST"})
      .then(results => results.json())
      .then(data => {
        if(data==='File is successfully uploaded'){
          setModaluploaded(true);
        console.log(data);
        }
      }).catch(err => {
        console.log(err);
      });
  }

  return (
    <div>
      <img src={cadenz} height={100} width={100} />
      <Button variant="contained" color="primary" style={{
        float: 'right', margin: '10px', backgroundBolor: 'red',
        backgroundImage: 'linear-gradient(to top right,  red, #f06d06 )'
      }}
      onClick={handleModalChange} >
        Deploy Model
      </Button>
      <Button type="file" variant="contained" color="primary" onClick={handlefileChange} style={{
        float: 'right', margin: '10px', backgroundBolor: 'red',
        backgroundImage: 'linear-gradient(to top right,  red, #f06d06 )'
      }} >
        <input
          onChange={changeHandler}
          type="file"
        />
        Upload File
      </Button>

     
     {isModaluploaded===true? <div style={{height:'500px',width:'700px',marginTop:'20px',marginLeft:'10px'}}>
      <span style={{fontWeight:'bold'}}>Attribute Mapping </span>
      <BasicTable />
      </div>:null}
      {modalData && modalData.data?<Paper  style={{height:'100px',width:'700px',marginTop:'20px',marginLeft:'10px',padding:'20px'}}>
      {modalData && modalData.data? <div><span>Deployment Time :</span><span  style={{border:' 2px solid lightgrey' ,padding:'2px'}}>{modalData.data['Deployment time']}</span></div>:null}
      {modalData && modalData.data? <div style={{marginTop:'20px'}}><span>Endpoint : </span><span style={{border:'2px solid lightgrey',padding:'2px'}}>{modalData.data.endpoint}</span></div>:null}
      </Paper>:null}
    </div>
  );
}

const mapStateToProps = state => ({
  login: state.login
});

// const mapDispatchToProps = { addCount };

export default connect(mapStateToProps)(Login);