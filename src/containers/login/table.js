import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

function createData(profileattr,mlattr) {
  return {profileattr,mlattr };
}

const rows = [
  createData('Application_band','APP_BAND'),
  createData('MLB_band', 'MLB_BAND'),
  createData('Behavioural_Score', 'BEHAV_SCORE4'),
  createData('Perfios_db_sme_band', 'PERFIOS_DEBIT_SME_BAND '),
  createData('laa_branchID', 'LAA_BRANCHID'),
  createData('Hub','Hub'),
  createData('City', 'CITY'),

];

export default function BasicTable() {
  const classes = useStyles();

  return (
      <div style={{height:'500px',width:'700px',marginTop:'5px'}}>
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell style={{fontWeight:'bold'}}>Profile attributes	</TableCell>
            <TableCell style={{fontWeight:'bold'}}>ML Features</TableCell>
            
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <TableRow key={row.profileattr}>
             
              <TableCell >{row.profileattr}</TableCell>
              <TableCell >{row.mlattr}</TableCell>
              
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
    </div>
  );
}
