import React from 'react';
import ReactDOM from 'react-dom';
import reduxStore from './store';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';
import './assets/styles/custom.scss';
import ReactBootStrap from './containers';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(
    <Provider store={reduxStore.store}>
    <ConnectedRouter history={reduxStore.history}>
            <ReactBootStrap />
    </ConnectedRouter>
</Provider>, document.getElementById('root'));

serviceWorker.register();
